import datetime
from django.test import TestCase
from django.utils import timezone

from django.core.urlresolvers import reverse

from polls.models import Poll


def create_poll(question, days):
    return Poll.objects.create(question=question, pub_date=timezone.now() + datetime.timedelta(days=days))


class PollViewTests(TestCase):
    def test_index_view_with_no_polls(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No polls are available')
        self.assertQuerysetEqual(response.context['latest_polls_list'], [])

    def test_index_view_with_a_past_poll(self):
        create_poll('Past poll', -0.5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_polls_list'],
            ['<Poll: Past poll>']
        )

    def test_index_view_with_a_very_old_poll(self):
        create_poll('Past poll', -200)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_polls_list'],
            []
        )

    def test_index_view_with_a_future_poll(self):
        create_poll('Future poll', 30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_polls_list'], [])
        self.assertContains(response, 'No polls are available')



class PollIndexDetailTest(TestCase):
    def test_detail_view_with_future_poll(self):
        future_poll = create_poll('future poll...', 10)
        response = self.client.get(reverse('polls:detail', args=(future_poll.id,)))
        self.assertEqual(response.status_code, 404)

    def test_detail_view_with_past_poll(self):
        past_poll = create_poll('future poll...', -10)
        response = self.client.get(reverse('polls:detail', args=(past_poll.id,)))
        self.assertEqual(response.status_code, 200)


class PollMethodTests(TestCase):

    def test_was_published_recently_with_future_poll(self):

        future_poll = Poll(pub_date=timezone.now() + datetime.timedelta(days=30))
        self.assertEqual(future_poll.was_published_recently(), False)

    def test_was_published_recently_with_recent_poll(self):
        recent_poll = Poll(pub_date=timezone.now() - datetime.timedelta(hours=3))
        self.assertEqual(recent_poll.was_published_recently(), True)

    def test_was_published_recently_with_recent_very_old_poll(self):
        very_old_poll = Poll(pub_date=timezone.now() - datetime.timedelta(days=10))
        self.assertEqual(very_old_poll.was_published_recently(), False)
