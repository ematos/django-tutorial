import datetime
from django.db import models
from django.utils import timezone


class Poll(models.Model):
    question = models.CharField(max_length=255)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.question

    def was_published_recently(self):
        now = timezone.now()
        return self.pub_date <= now and self.pub_date >= now - datetime.timedelta(days=1)


def recent_poll(max_results, days=1):
    return Poll.objects.filter(
        pub_date__lte=timezone.now()
    ).filter(pub_date__gte=timezone.now() - datetime.timedelta(days=days)).order_by('-pub_date')[:max_results]


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice_text = models.CharField(max_length=255)
    votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.choice_text
